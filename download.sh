#!/bin/sh

set -x
set -e

# Download Helm and helm-push
HELM_VERSION="$(cat HELM_VERSION)"
curl -fLSs "https://get.helm.sh/helm-$HELM_VERSION-linux-amd64.tar.gz" | tar -xz --strip-components=1 linux-amd64/helm
./helm plugin uninstall cm-push ||:
./helm plugin install https://github.com/chartmuseum/helm-push.git

