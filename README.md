# Keycloak Operator Legacy Helm chart

This is an Helm chart for [Keycloak Operator Legacy](https://github.com/keycloak/keycloak-operator).

## Usage

```shell
helm repo add kubitus-keycloak-operator-legacy https://gitlab.com/api/v4/projects/50776050/packages/helm/stable

helm install kubitus-keycloak-operator-legacy/keycloak-operator-legacy
```

Available values can be seen [here](values.yaml), or retrieved with:

```shell
helm show values kubitus-keycloak-operator-legacy/keycloak-operator-legacy
```
