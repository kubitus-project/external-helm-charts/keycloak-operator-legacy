{{/*
Expand the name of the chart.
*/}}
{{- define "keycloak-operator-legacy.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "keycloak-operator-legacy.fullname" -}}
{{- if .Values.fullnameOverride }}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- $name := default .Chart.Name .Values.nameOverride }}
{{- if contains $name .Release.Name }}
{{- .Release.Name | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" }}
{{- end }}
{{- end }}
{{- end }}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "keycloak-operator-legacy.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Common labels
*/}}
{{- define "keycloak-operator-legacy.labels" -}}
helm.sh/chart: {{ include "keycloak-operator-legacy.chart" . }}
{{ include "keycloak-operator-legacy.selectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}

{{/*
Selector labels
*/}}
{{- define "keycloak-operator-legacy.selectorLabels" -}}
app.kubernetes.io/name: {{ include "keycloak-operator-legacy.name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}

{{/*
Create the name of the service account to use
*/}}
{{- define "keycloak-operator-legacy.serviceAccountName" -}}
{{- if .Values.serviceAccount.create }}
{{- default (include "keycloak-operator-legacy.fullname" .) .Values.serviceAccount.name }}
{{- else }}
{{- default "default" .Values.serviceAccount.name }}
{{- end }}
{{- end }}

{{/*
Role rules
*/}}
{{- define "keycloak-operator-legacy.roleRules" -}}
- apiGroups:
  - ""
  resources:
  - pods
  - services
  - services/finalizers
  - endpoints
  - persistentvolumeclaims
  - events
  - configmaps
  - secrets
  verbs:
  - list
  - get
  - create
  - patch
  - update
  - watch
  - delete
- apiGroups:
  - apps
  resources:
  - deployments
  - daemonsets
  - replicasets
  - statefulsets
  verbs:
  - list
  - get
  - create
  - update
  - watch
  - delete
- apiGroups:
  - batch
  resources:
  - cronjobs
  - jobs
  verbs:
  - list
  - get
  - create
  - update
  - watch
- apiGroups:
  - route.openshift.io
  resources:
  - routes/custom-host
  verbs:
  - create
- apiGroups:
  - route.openshift.io
  resources:
  - routes
  verbs:
  - list
  - get
  - create
  - update
  - watch
- apiGroups:
  - networking.k8s.io
  resources:
  - ingresses
  verbs:
  - list
  - get
  - create
  - update
  - watch
- apiGroups:
  - monitoring.coreos.com
  resources:
  - servicemonitors
  - prometheusrules
  verbs:
  - list
  - get
  - create
  - update
  - watch
- apiGroups:
  - integreatly.org
  resources:
  - grafanadashboards
  verbs:
  - get
  - list
  - create
  - update
  - watch
- apiGroups:
  - apps
  resourceNames:
  - keycloak-operator
  resources:
  - deployments/finalizers
  verbs:
  - update
- apiGroups:
  - policy
  resources:
  - poddisruptionbudgets
  verbs:
  - get
  - list
  - create
  - update
  - watch
- apiGroups:
  - keycloak.org
  resources:
  - keycloaks
  - keycloaks/status
  - keycloaks/finalizers
  - keycloakrealms
  - keycloakrealms/status
  - keycloakrealms/finalizers
  - keycloakclients
  - keycloakclients/status
  - keycloakclients/finalizers
  - keycloakbackups
  - keycloakbackups/status
  - keycloakbackups/finalizers
  - keycloakusers
  - keycloakusers/status
  - keycloakusers/finalizers
  verbs:
  - get
  - list
  - update
  - watch
{{- end }}
